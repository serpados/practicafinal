% PRACTICA FINAL COVID-19 CASOS CONFIRMADOS Y NUMERO DE MUERTOS
% K-MEANS
% SERAFIN MARTIN COTANO

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ejemplo   a l g o r i t m o   d e   l a s   k - m e d i a s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all, clear clc   % cerrrar  ventanas gr�ficas 
                        % borrar memoria y  consola
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% G e n e r a c i � n   d e   d a t o s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('agregadosPaisesCasosDiarios.mat')  % carga de datos Cases
load('agregadosPaisesMuertosDiarios.mat')  % carga de datos Deaths
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% E j e c u c i � n   a l g o r i t m o    d e    l a s 
%                      K - m e d i a s 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%                       
% p a r � m e t r o s   d e   e n t r a d a
% X matriz de datos, filas individuos, columnas atributos
% K numero de grupos a conformar
% 'Replicates' n�mero de repeticiones, 1 en este caso 
% 'Distance'  distancia usada, 'sqEuclidean'->el cuadrado de 
%             la eucl�dea, tambi�n se puede elegir 'city'->L1
%%%%%%%
% p a r � m e t r o s   d e   s a l i d a              
% cidx(i) devuelve el conglogmerado al que pertenece el dato i
% ctrs    centroides de los grupos
% sumd    suma de la distancia intracluster 
% D       matriz de distancia de cada objeto (fila) a 
%         cada centroide (columna)

% Cases Algoritmo Kmeans
optsCases = statset('Display','iter','MaxIter',100);
[cidxCases, ctrsCases,sumdCases,DCases] = kmeans(agregadosPaisesCasosDiarios, 4,'Replicates',10, 'Distance',...
                           'sqEuclidean','Options',optsCases);
% Deaths Algoritmo Kmeans
optsDeaths = statset('Display','iter','MaxIter',100);
[cidxDeaths, ctrsDeaths,sumdDeaths,DDeaths] = kmeans(agregadosPaisesMuertosDiarios, 3,'Replicates',10, 'Distance',...
                           'sqEuclidean','Options',optsDeaths);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% R e p r e s e n t a c i � n   d e    l a    s o l u c i � n
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%CASOS 
 figure (1)
 plot([agregadosPaisesCasosDiarios]);
 xlabel('x_1','fontsize',18),ylabel('x_2','fontsize',18)
 legend('Grupo 1','Grupo 2','Grupo 3','Grupo 4'),axis('square'), box on
 title('Algoritmo K-medias Cases','fontsize',18)
 figure (2)
 plot([agregadosPaisesMuertosDiarios]);
 xlabel('x_1','fontsize',18),ylabel('x_2','fontsize',18)
 legend('Grupo 1','Grupo 2','Grupo 3'),axis('square'), box on
 title('Algoritmo K-medias Deaths','fontsize',18)
 
%save ('agregadosPaisesCasosDiarios.mat','agregadosPaisesCasosDiarios') %guardar matriz de datos Cases
%save ('agregadosPaisesMuertosDiarios.mat','agregadosPaisesMuertosDiarios') %guardar matriz de datos Cases




