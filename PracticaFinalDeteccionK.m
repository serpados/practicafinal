%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% calculo del numero de cluster K
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all, clear clc   % cerrrar  ventanas gr�ficas 
                        % borrar memoria y  consola
%load('agregadosPaisesCasosDiarios.mat')  % carga de datos Cases
%load('agregadosPaisesMuertosDiarios.mat')  % carga de datos Deaths
Kmax=10;
m=2;                       % par�metro de fcm, 2 es el defecto
MaxIteraciones=100;        % n�mero de iteraciones
Tolerancia= 1e-05;          % tolerancia en el criterio de para
Visualizacion=0;           % 0/1
opciones=[m,MaxIteraciones,Visualizacion];

for K=2:Kmax
    [cidxCasos] = kmeans(agregadosPaisesCasosDiarios, K,'Replicates',10);
    [Bic_KCasos,xiCasos]=BIC(K,cidxCasos,agregadosPaisesCasosDiarios);
    BICKCasos(K)=Bic_KCasos;
end
figure(1)

plot(2:K',BICKCasos(2:K)','s-','MarkerSize',6,...
     'MarkerEdgeColor','r', 'MarkerFaceColor','r')
xlabel('K','fontsize',18)      % etiquetado del eje-x
ylabel('BIC(K)','fontsize',18) % etiquetado del eje-y
title('Algoritmo K-medias Cases','fontsize',18)
for K=2:Kmax
    [cidxDeaths] = kmeans(agregadosPaisesMuertosDiarios, K,'Replicates',10);
    [Bic_KDeaths,xiDeaths]=BIC(K,cidxDeaths,agregadosPaisesMuertosDiarios);
    BICKDeaths(K)=Bic_KDeaths;
end
figure(2)

plot(2:K',BICKDeaths(2:K)','s-','MarkerSize',6,...
     'MarkerEdgeColor','r', 'MarkerFaceColor','r')
xlabel('K','fontsize',18)      % etiquetado del eje-x
ylabel('BIC(K)','fontsize',18) % etiquetado del eje-y
title('Algoritmo K-medias Deaths','fontsize',18)
save ('agregadosPaisesCasosDiarios.mat','agregadosPaisesCasosDiarios') %guardar matriz de datos Cases
save ('agregadosPaisesMuertosDiarios.mat','agregadosPaisesMuertosDiarios') %guardar matriz de datos Cases