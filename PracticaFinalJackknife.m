%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D e t e c c i � n   d e   o b s e r v a c i o n e s  
% i n f l u y e  n t e s :  M � t o d o  d e  J A C K N I F E
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all,clear clc;
load('agregadosPaisesCasosDiarios.mat')  % carga de datos Cases
load('agregadosPaisesMuertosDiarios.mat')  % carga de datos Deaths
N=size(agregadosPaisesMuertosDiarios,1);
m=2;
MaxIteraciones=100; % n�mero de iteraciones
Tolerancia= 1e-5;   % tolerancia en el criterio de parada
Visualizacion=0;    % 0/1
opciones=[m,MaxIteraciones,Visualizacion];

%CASOS

K=4;             % n�mero de clusters
for i=1:N-1
    agregadosPaisesCasosDiarios_sin_i=[agregadosPaisesCasosDiarios(1:(i-1),:);agregadosPaisesCasosDiarios((i+1):N,:)]; % eliminacion de la observaci�n i
    [centerCasos,UCasos,obj_fcnCasos] = fcm(agregadosPaisesCasosDiarios_sin_i, K,opciones);
    SSECasos(i)=sum(obj_fcnCasos); % La suma del resultado del algoritmo nos da el valor SSE(i) para obtener los outliers   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% D e t e c c i � n   v i s u a l
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(1)
plot(SSECasos)
xlabel('Dato i','fontsize',18)
ylabel('SSE eliminando el dato i','fontsize',18)
title('Algoritmo K-medias Cases','fontsize',18)
grid on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% D e t e c c i � n   a n a l � t i c a 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sigma=std(SSECasos);    % desviaci�n t�pica
mu=mean(SSECasos);      % media 
umbral=1.98;       % umbral =2 para distribuciones normales
                   % umbral =3 para cualquier ditribuci�n
outliersCases=[];       % inicializaci�n del vector de outliers
for i=1:N-1
    if abs(SSECasos(i)-mu)>umbral*sigma
        outliersCases=[outliersCases,i];
    end
end
outliersCases            % impresi�n por pantalla de los outliers 

% se�alamos los outliers en la gr�fica
hold on
for i=1:length(outliersCases)
    dato=outliersCases(i);
    plot(dato,SSECasos(dato),'bo',...
    'MarkerSize',6,'MarkerEdgeColor','b', 'MarkerFaceColor','b')
    text(dato,SSECasos(dato),'OutlierCase','fontsize',18);
end

%DEATHS

K=3;             % n�mero de clusters
for i=1:N-1
    agregadosPaisesMuertosDiarios_sin_i=[agregadosPaisesMuertosDiarios(1:(i-1),:);agregadosPaisesMuertosDiarios((i+1):N,:)]; % eliminacion de la observaci�n i
    [centerDeaths,UDeaths,obj_fcnDeaths] = fcm(agregadosPaisesMuertosDiarios_sin_i, K,opciones);
    SSEDeaths(i)=sum(obj_fcnDeaths); % La suma del resultado del algoritmo nos da el valor SSE(i) para obtener los outliers   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% D e t e c c i � n   v i s u a l
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(2)
plot(SSEDeaths)
xlabel('Dato i','fontsize',18)
ylabel('SSE eliminando el dato i','fontsize',18)
title('Algoritmo K-medias Deaths','fontsize',18)
grid on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% D e t e c c i � n   a n a l � t i c a 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sigma=std(SSEDeaths);    % desviaci�n t�pica
mu=mean(SSEDeaths);      % media 
umbral=1.98;       % umbral =2 para distribuciones normales
                   % umbral =3 para cualquier ditribuci�n
outliersDeaths=[];       % inicializaci�n del vector de outliers
for i=1:N-1
    if abs(SSEDeaths(i)-mu)>umbral*sigma
        outliersDeaths=[outliersDeaths,i];
    end
end
outliersDeaths            % impresi�n por pantalla de los outliers 

% se�alamos los outliers en la gr�fica
hold on
for i=1:length(outliersDeaths)
    dato=outliersDeaths(i);
    plot(dato,SSEDeaths(dato),'bo',...
    'MarkerSize',6,'MarkerEdgeColor','b', 'MarkerFaceColor','b')
    text(dato,SSEDeaths(dato),'OutlierDeath','fontsize',18);
end